/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moc.model;

/**
 *
 * @author Raim
 */
public class MessageStore {
  private String message;

  public MessageStore() {
    message = "default message;";
  }
  
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
  
}
