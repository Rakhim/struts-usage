/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moc.action;

import com.moc.model.MessageStore;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author Raim
 */
public class HelloWorldAction extends ActionSupport {
  private static final long serialVersionUID = 1L;
  private MessageStore messageStore;
  
  @Override
  public String execute() throws Exception {
    messageStore = new MessageStore();
    return SUCCESS;
  }

  public MessageStore getMessageStore() {
    return messageStore;
  }

  public void setMessageStore(MessageStore messageStore) {
    this.messageStore = messageStore;
  }
  
}
